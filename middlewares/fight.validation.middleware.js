const { fight } = require('../models/fighter');
const { filterObj, isPropertiesExistsInObj, isEmpty } = require('../helpers/validationHelper');

const createFightValid = (req, res, next) => {
  try {
    const requiredFields = ['fighter1', 'fighter2', 'log'];
    if (!isPropertiesExistsInObj(requiredFields, req.body)) {
      throw "Some properties don't exists"
    }
    req.body = filterObj(req.body, requiredFields);
    if (!isValidLog(req.body.log)) {
      throw 'Invalid log'
    }

  } catch (err) {
    next(err)
  } finally {
    next()
  }

}

const updateFightValid = (req, res, next) => {
  try {
    const aviableproperties = ['fighter1', 'fighter2', 'log'];
    req.body = filterObj(req.body, aviableproperties);
    if (isEmpty(req.body)) {
      throw 'Wrong properties'
    }
    if (req.body.hasOwnProperty('log') && !isValidLog(req.body.log)) {
      throw 'Invalid log'
    }
  } catch (err) {
    next(err)
  } finally {
    next()
  }
}


function isValidLog(log) {
  arr = ['fighter1Shot', 'fighter2Shot', 'fighter1Health', 'fighter2Health'];
  for (let i = 0; i < arr.length; i++) {
    if (!log.hasOwnProperty(arr[i])) {
      return false
    }
    if (typeof (log[arr[i]]) !== 'number') {
      return false
    }
  }
  return true
}



exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;