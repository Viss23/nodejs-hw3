const { fighter } = require('../models/fighter');
const { filterObj, isPropertiesExistsInObj, isEmpty } = require('../helpers/validationHelper');

const createFighterValid = (req, res, next) => {
    try {
        if (req.body.hasOwnProperty('id') ){    
            throw 'Id can not be in body'
        }

        const requiredFields = ['name', 'power', 'defense','health'];
        if (!isPropertiesExistsInObj(requiredFields, req.body)) {
            throw "Some properties don't exists"
        }
        if ( !req.body.health){
            req.body.health = 100;
        }
        req.body = filterObj(req.body, requiredFields);

        if (!isValidDefense(req.body.defense)) {
            throw 'Invalid defense'
        }
        if (!isValidPower(req.body.power)) {
            throw 'Invalid power'
        }
    } catch (err) {
        next(err)
    } finally {
        next()
    }
}

const updateFighterValid = (req, res, next) => {
    try {
        const aviableproperties = ['name', 'power', 'defense'];
        req.body = filterObj(req.body, aviableproperties);
        if (isEmpty(req.body)) {
            throw 'Wrong properties'
        }
        if (req.body.hasOwnProperty('power') && !isValidPower(req.body.power)) {
            throw 'Invalid power'
        }
        if (req.body.hasOwnProperty('defense') && !isValidDefense(req.body.defense)) {
            throw 'Invalid defense'
        }
    } catch (err) {
        next(err)
    } finally {
        next()
    }
}


function isValidDefense(defense) {
    return (typeof (defense) === 'number' && defense > 0 && defense <= 10)
}

function isValidPower(power) {
    return (typeof (power) === 'number' && power > 0 && power < 100)
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;

