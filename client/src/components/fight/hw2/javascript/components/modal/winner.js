import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const bodyElement = createElement({
    tagName: "div",
    className: "fighterWinBody"
  })

  const span = createElement({
    tagName: "span",
    className: "fighterText"
  })

  span.innerHTML= `${fighter.name} WINS`;

  bodyElement.append(span);

  showModal({title:'K.O.',bodyElement:bodyElement, onClose:() =>document.location.reload(true)});

}
