import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
	return new Promise((resolve) => {

		const changeFirstFighterHpBar = updateLeftFighterHpBar(firstFighter.health);
		const changeSecondFighterHpBar = updateRightFighterHpBar(secondFighter.health);
		let firstPlayerComboCanBeUsedInTime = 0;
		let secondPlayerComboCanBeUsedInTime = 0;
		const log = {
						 "fighter1Shot": 0,
						 "fighter2Shot": 0,
						 "fighter1Health": 0,
						 "fighter2Health": 0	
		}
		

		let pressed = new Set();

		const fightCallback1 = function (event) {
			pressed.add(event.code);

      let firstFighterAttack = pressed.has(controls.PlayerOneAttack) && !pressed.has(controls.PlayerOneBlock) && 
        !pressed.has(controls.PlayerTwoBlock);
      let secondFighterAttack = pressed.has(controls.PlayerTwoAttack) && !pressed.has(controls.PlayerTwoBlock) && 
        !pressed.has(controls.PlayerOneBlock);
			let firstFighterUseCombo =
				controls.PlayerOneCriticalHitCombination.every((element) => pressed.has(element)) &&
        firstPlayerComboCanBeUsedInTime < Math.floor(Date.now() / 1000) &&
        !pressed.has(controls.PlayerOneBlock);
			let secondFighterUseCombo =
				controls.PlayerTwoCriticalHitCombination.every((element) => pressed.has(element)) &&
        secondPlayerComboCanBeUsedInTime < Math.floor(Date.now() / 1000) &&
        !pressed.has(controls.PlayerTwoBlock);

			if (firstFighterUseCombo) {
				firstPlayerComboCanBeUsedInTime = Math.floor(Date.now() / 1000) + 10;
				log.fighter1Shot=log.fighter1Shot +1;
				changeSecondFighterHpBar(secondFighter, specialAttack(firstFighter));
				if (isFighterDead(secondFighter)) {
					document.removeEventListener('keydown', fightCallback1);
					document.removeEventListener('keyup', fightCallback2);
					log.fighter1Health=firstFighter.health;
					log.fighter2Health=secondFighter.health;
					resolve({winner:firstFighter,log:log});
				}
			} else if (secondFighterUseCombo) {
				secondPlayerComboCanBeUsedInTime = Math.floor(Date.now() / 1000) + 10;
				log.fighter2Shot=log.fighter2Shot +1;
				changeFirstFighterHpBar(firstFighter, specialAttack(secondFighter));
				if (isFighterDead(firstFighter)) {
					document.removeEventListener('keydown', fightCallback1);
					document.removeEventListener('keyup', fightCallback2);
					log.fighter1Health=firstFighter.health;
					log.fighter2Health=secondFighter.health;
					resolve({winner:firstFighter,log:log});
				}
			} else if (firstFighterAttack) {
				changeSecondFighterHpBar(secondFighter, getDamage(firstFighter, secondFighter));
				log.fighter1Shot=log.fighter1Shot +1;
				if (isFighterDead(secondFighter)) {
					document.removeEventListener('keydown', fightCallback1);
					document.removeEventListener('keyup', fightCallback2);
					log.fighter1Health=firstFighter.health;
					log.fighter2Health=secondFighter.health;
					resolve({winner:firstFighter,log:log});
				}
			} else if (secondFighterAttack) {
				changeFirstFighterHpBar(firstFighter, getDamage(secondFighter,firstFighter));
				log.fighter2Shot=log.fighter2Shot +1;
				if (isFighterDead(firstFighter)) {
					document.removeEventListener('keydown', fightCallback1);
					document.removeEventListener('keyup', fightCallback2);
					log.fighter1Health=firstFighter.health;
					log.fighter2Health=secondFighter.health;
					resolve({winner:firstFighter,log:log});
				}
			}
		};

		const fightCallback2 = function (event) {
			pressed.delete(event.code);
		};

		document.addEventListener('keydown', fightCallback1);
		document.addEventListener('keyup', fightCallback2);
	});
}

export function getDamage(attacker, defender) {
	const damage = getHitPower(attacker) - getBlockPower(defender);
	return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
	const criticalHitChance = Math.random() + 1;
	const hitPower = fighter.power * criticalHitChance;
	return hitPower;
}

export function getBlockPower(fighter) {
	const dodgeChance = Math.random() + 1;
	const blockPower = fighter.defense * dodgeChance;
	return blockPower;
}

export function specialAttack(attacker) {
	let damage = attacker.power * 2;
	return damage;
}

export function updateLeftFighterHpBar(maxHp) {
	return function (fighter, damage) {
		let currentHp = fighter.health - damage;
		 if (currentHp <0) currentHp=0
    fighter.health = currentHp;
		let percentOfMaxHp = (currentHp / maxHp) * 100;
		if (percentOfMaxHp < 0) percentOfMaxHp = 0;
		document.getElementById('left-fighter-indicator').style.width = `${percentOfMaxHp}%`;
	};
}

export function updateRightFighterHpBar(maxHp) {
	return function (fighter, damage) {
		let currentHp = fighter.health - damage;
		if (currentHp <0) currentHp=0
		fighter.health = currentHp;
		let percentOfMaxHp = (currentHp / maxHp) * 100;
		if (percentOfMaxHp < 0) percentOfMaxHp = 0;
		document.getElementById('right-fighter-indicator').style.width = `${percentOfMaxHp}%`;
	};
}

export function isFighterDead(fighter) {
	if (fighter.health <= 0) {
		return true;
	}
	return false;
}