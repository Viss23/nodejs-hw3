import React from 'react';

import { getFighters } from '../../services/domainRequest/fightersRequest';
import NewFighter from '../newFighter';
import Fighter from '../fighter';
import { Button } from '@material-ui/core';
import Table from '../table';

import {renderArena} from './hw2/javascript/components/arena.js'

import './fight.css'
import { getFights } from '../../services/domainRequest/fightRequest';

class Fight extends React.Component {
    state = {
        fighters: [],
        fighter1: null,
        fighter2: null,
        fights: null,
        showStats:false
    };

    async componentDidMount() {
        const fighters = await getFighters();
        if(fighters && !fighters.error) {
            this.setState({ fighters });
        }
        const fights = await getFights();
        if (fights && !fights.error){
            this.setState({fights: this.parseFight(fights)})
        }
    }

    onFightStart = () => {
        renderArena([this.state.fighter1,this.state.fighter2]);
    }

    onCreate = (fighter) => {
        this.setState({ fighters: [...this.state.fighters, fighter] });
    }

    onFighter1Select = (fighter1) => {
        this.setState({fighter1 });
    }

    onFighter2Select = (fighter2) => {
        this.setState({ fighter2 });
    }

    getFighter1List = () => {
        const { fighter2, fighters } = this.state;
        if(!fighter2) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter2.id);
    }

    getFighter2List = () => {
        const { fighter1, fighters } = this.state;
        if(!fighter1) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter1.id);
    }

    parseFight = (arrayOfObj) =>{
        for(let i=0;i<arrayOfObj.length;i++){
            let log=arrayOfObj[i].log;
            delete arrayOfObj[i].log
            arrayOfObj[i]={...arrayOfObj[i], ...log}
          }
          return arrayOfObj
    }

    showFightsStats = () => {
       this.setState({showStats: !this.state.showStats});    
    }

    render() {
        const  { fighter1, fighter2 } = this.state;
        return (
            <div id="wrapper">
                <NewFighter onCreated={this.onCreate} />
                <div className="btn-wrapper">
                        <Button onClick={this.showFightsStats} variant="contained" color="primary">Show/Hide fights stats</Button>
                </div>
                {this.state.fights &&this.state.showStats && <Table data={this.state.fights}></Table>}
                <div id="figh-wrapper">
                    <Fighter selectedFighter={fighter1} onFighterSelect={this.onFighter1Select} fightersList={this.getFighter1List() || []} />
                    <div className="btn-wrapper">
                        <Button onClick={this.onFightStart} variant="contained" color="primary">Start Fight</Button>
                    </div>
                    <Fighter selectedFighter={fighter2} onFighterSelect={this.onFighter2Select} fightersList={this.getFighter2List() || []} />
                </div>
            </div>
        );
    }
}

export default Fight;