const { Router } = require('express');
const FightService = require('../services/fightService');
const { createFightValid, updateFightValid } = require('../middlewares/fight.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights

router.get('/', getAll, responseMiddleware); // +
router.get('/:id', getById, responseMiddleware); //+
router.post('/', createFightValid, create, responseMiddleware); //+
router.put('/:id', updateFightValid, update, responseMiddleware) //+
router.delete('/:id', _delete, responseMiddleware);

function create(req, res, next) {
  FightService.create(req.body)
    .then(() => res.json({}))
    .catch(err => next(err));
}

function getAll(req, res, next) {
  FightService.getAll()
    .then(users => res.json(users))
    .catch(err => next(err))
}

function getById(req, res, next) {
  FightService.getById(req.params.id)
    .then(user => res.json(user))
    .catch(err => next(err))
}

function update(req, res, next) {
  FightService.update(req.params.id, req.body)
    .then(() => res.json({}))
    .catch(err => next(err));
}

function _delete(req, res, next) {
  FightService.delete(req.params.id)
    .then(() => res.json({}))
    .catch(err => next(err));
}

module.exports = router;