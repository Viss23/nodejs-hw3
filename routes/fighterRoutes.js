const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/', getAll, responseMiddleware);//+
router.get('/:id', getById, responseMiddleware);//+
router.post('/', createFighterValid, create, responseMiddleware);//+
router.put('/:id', updateFighterValid, update, responseMiddleware);//+
router.delete('/:id', _delete, responseMiddleware)//+

function create(req, res, next) {
  FighterService.create(req.body)
    .then((result) => res.json(result))
    .catch(err => next(err));
}

function getAll(req, res, next) {
  FighterService.getAll()
    .then(fighters => res.json(fighters))
    .catch(err => next(err))
}

function getById(req, res, next) {
  FighterService.getById(req.params.id)
    .then((fighter) => res.json(fighter))
    .catch(err => next(err))
}

function update(req, res, next) {
  FighterService.update(req.params.id, req.body)
    .then((updatedFighter) => res.json(updatedFighter))
    .catch(err => next(err));
}

function _delete(req, res, next) {
  FighterService.delete(req.params.id)
    .then((deletedUser) => res.json(deletedUser))
    .catch(err => next(err));
}

module.exports = router;