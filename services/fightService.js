const { FightRepository } = require('../repositories/fightRepository');
const { FighterRepository } = require('../repositories/fighterRepository');

class FightService {
    // OPTIONAL TODO: Implement methods to work with fights
    async create(data) {
       return await FightRepository.create(data)
    }

    async getAll() {
        return await FightRepository.getAll();
    }

    async getById(id) {
        const fight = await FightRepository.getOne({ id: id });
        if (!fight) throw 'Fight not found';
        return fight
    }

    async update(id, dataToUpdate) {
        const oldFightdata = await FightRepository.getOne({ id: id });
        if (!oldFightdata) throw 'Fight not found';
        if (dataToUpdate.hasOwnProperty('fighter1')) {
            if (!FighterRepository.getOne({ id: dataToUpdate.fighter1 })) {
                throw 'Fighter with such id does not exist'
            }
        }
        if (dataToUpdate.hasOwnProperty('fighter2')) {
            if (!FighterRepository.getOne({ id: dataToUpdate.fighter2 })) {
                throw 'Fighter with such id does not exist'
            }
        }
        const newFightdata = { ...oldFightdata, ...dataToUpdate };
        return await FightRepository.update(id, newFightdata);
    }

    async delete(id) {
        const fight = await FightRepository.getOne({ id: id });
        if (!fight) throw 'Fight not found';
        return await FightRepository.delete(id)
    }
}

module.exports = new FightService();