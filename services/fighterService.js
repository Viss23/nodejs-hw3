const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    async create(data) {
        if (await FighterRepository.getOne({ name: data.name })) {
            throw "Fighter with such name is already exists"
        }
       return await FighterRepository.create(data)
    }

    async getAll() {
        return await FighterRepository.getAll();
    }

    async getById(id) {
        const fighter = await FighterRepository.getOne({ id: id });
        if (!fighter) throw 'Fighter not found';
        return fighter
    }

    async update(id, dataToUpdate) {
        const oldFighterdata = await FighterRepository.getOne({ id: id });
        if (!oldFighterdata) throw 'Bad request';
        if (dataToUpdate.hasOwnProperty('name')) {
            if (FighterRepository.getOne({ name: dataToUpdate.name })) {
                throw 'Fighter with such name is already exists'
            }
        }
        const newFighterdata = { ...oldFighterdata, ...dataToUpdate };
        return await FighterRepository.update(id, newFighterdata);
    }

    async delete(id) {
        const fighter = await FighterRepository.getOne({ id: id });
        if (!fighter) throw 'Fighter not found';
        return await FighterRepository.delete(id)
    }

}

module.exports = new FighterService();